package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        isNull(x,y);
        if (x.isEmpty() ){
            return true;
        } else if(y.isEmpty()){
            return false;
        } else {
            return checkArray(x,y);
        }
    }

    private void isNull(List list1, List list2){
        if (list1 == null||list2 == null) {
            throw new IllegalArgumentException();
        }
    }

    private boolean checkArray(List x, List y){
        int count =0;
        for(int i = 0; i<x.size(); i++){
            if (count>=y.size()){
                return false;
            }
            boolean stillWorks = false;
            for(int j = count; j<y.size(); j++){
                count++;
                if (x.get(i) == y.get(j)){
                    stillWorks = true;
                    break;
                }
            }
            if (!stillWorks){
                return false;
            }
            if (stillWorks && i+1 == x.size()){
                return true;
            }
        }
        return false;
    }
}
