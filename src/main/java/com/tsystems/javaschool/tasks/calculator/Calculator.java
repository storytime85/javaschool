package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.Collections;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        if (statement == null || statement.equals("")){
            return null;
        } else if (!checkBrackets(statement)){
            return null;
        } else if (statement.contains(",") || statement.contains(" ")){
            return null;
        } else {
            String result = solveBrackets(0,statement);
            if (result == null){
                return null;
            } else {
                //if number is natural, delete mantissa, else string to double, then round up to 4 decimals, then back to string
                double temp = Double.parseDouble(result);
                if (temp == (int)temp){
                    return Integer.toString((int)temp);
                } else {
                    return Double.toString(Math.round(temp * 10000.0) / 10000.0);
                }
            }
        }
    }

    private boolean checkBracketsExistance(String statement){
        for (int i = 0; i<statement.length();i++){
            String oneChar = Character.toString(statement.charAt(i));
                if (oneChar.equals("(") | oneChar.equals(")")){
                    return true;
                }
        }
        return false;
    }

    private boolean checkBrackets(String string){
        int outerCount =0;
        int innerCount =0;
        int lastIndex =0;
        if (checkBracketsExistance(string)) {
            for (int i = 0; i < string.length(); i++) {
                String s = Character.toString(string.charAt(i));
                if (s.equals("(")) {
                    outerCount++;
                    lastIndex = i;
                } else if (s.equals(")")) {
                    if (outerCount == 0) {
                        return false;
                    }
                    innerCount++;
                    lastIndex = i;
                }

            }
        }
        if (outerCount == 0 && innerCount == 0 && lastIndex ==0){
            return true;
        } else if (Character.toString(string.charAt(lastIndex)).equals(")")){
            return outerCount == innerCount;
        } else {
            return false;
        }

    }

    private String solveBrackets(int level, String statement){
        String solvedString = statement;
        int curLevel = 0;
        ArrayList<String> subbrackets = new ArrayList<>();
        boolean stillDo = false;
        if (checkBracketsExistance(statement)) {
            for (int i = 0; i < statement.length(); i++) {
                String oneChar = Character.toString(statement.charAt(i));
                if (oneChar.equals("(")) {
                    curLevel++;
                    continue;
                } else if (oneChar.equals(")")) {
                    curLevel--;
                    stillDo = false;
                    continue;
                }
                if (level < curLevel) {
                    if (!stillDo) {
                        stillDo = true;
                        subbrackets.add("");
                    }
                    subbrackets.set(subbrackets.size() - 1, subbrackets.get(subbrackets.size() - 1) + oneChar);
                }
            }
            for (String s : subbrackets){
                String result = solveBrackets(level+1, s);
                solvedString = solvedString.replace("(" + s + ")", result);
            }
            solvedString = solvedString.replace("+-","-");
            solvedString = solvedString.replace("--","+");

        }
        return doMath(solvedString);
    }

    private char getPrevious(String tempString, int i){
        if (i!=0){
            return tempString.charAt(i-1);
        } else {
            return tempString.charAt(i);
        }
    }

    private String doMath(String statement){
        ArrayList<String> operators = new ArrayList<>();
        ArrayList<Double> operands = new ArrayList<>();
        ArrayList<String> stringOperands = new ArrayList<>();
        String tempString ="";
        for (int i = 0; i<statement.length();i++){
            char s = statement.charAt(i);
            char prev = getPrevious(statement,i);
            if (s == '-' & (Character.isDigit(prev))){
                tempString+="+-";
            } else {
                tempString+=s;
            }
        }
        String tempNumber = "";
        boolean isWriting = false;

        for (int i =0; i<tempString.length(); i++){
            char s = tempString.charAt(i);
            char prev = getPrevious(tempString,i);
            if (s=='-' & i==0){
                tempNumber = tempNumber + s;
                isWriting = true;
            } else if (s=='-' & (prev == '+' | prev == '/' | prev == '*')){
                tempNumber = tempNumber + s;
                isWriting = true;
            } else if ((Character.isDigit(s) | s == '.')){
                isWriting = true;
                tempNumber = tempNumber + s;
            } else if (isWriting & !(Character.isDigit(s))){
                isWriting = false;
                stringOperands.add(tempNumber);
                tempNumber = "";
            }
            if (isWriting & (i+1 == tempString.length())){
                isWriting = false;
                stringOperands.add(tempNumber);
                tempNumber = "";
            }
        }

        ArrayList<String> tempList = new ArrayList<String>(stringOperands);
        Collections.reverse(tempList);
        for (int i = 0; i<tempList.size(); i++){
            tempString = tempString.replaceAll(tempList.get(i),"");
        }

        for (String s : stringOperands){
            try {
                operands.add(Double.parseDouble(s));
            } catch (IllegalArgumentException e){
                return null;
            }
        }
        for(int i =0; i<tempString.length();i++){
            char s = tempString.charAt(i);
            if (s == '+' | s == '-' | s== '/' | s == '*'){
                operators.add(Character.toString(s));
            }
        }

        tempString = tempString.replaceAll("[-+*/]","");
        if (!tempString.equals("")){
            return null;
        }

        if (operands.size() == operators.size()+1){
            int shift =0;
            ArrayList<Integer> operatorIndexes = new ArrayList<>();
            for (int i = 0; i < operators.size(); i++){
                int f =i + 1 - shift;
                if (operators.get(i).equals("/")){
                    if (operands.get(i+1) == 0){
                        return null;
                    } else {
                        operands.set(i-shift,operands.get(i - shift) / operands.get(i + 1 - shift));
                        operands.remove(i + 1 - shift);
                        operatorIndexes.add(i);
                        shift++;
                    }
                } else if (operators.get(i).equals("*")){
                    operands.set(i - shift, operands.get(i - shift) * operands.get(i + 1 - shift));
                    operands.remove(i + 1 - shift);
                    operatorIndexes.add(i);
                    shift++;
                }
            }
            for (int i =0; i<operatorIndexes.size();i++){
                operators.remove(operatorIndexes.get(i) -i);
            }
            if (operators.size()==0){
                return Double.toString(operands.get(0));
            }
            Double tempResult = null;
            for (int i = 0; i <operators.size(); i++){
                if (i==0){
                    tempResult = operands.get(0);
                }
                if (operators.get(i).equals("+")){
                    tempResult+=operands.get(i+1);
                } else if (operators.get(i).equals("-")){
                    tempResult-=operands.get(i+1);
                }
            }
            return Double.toString(tempResult);
        } else {
            return null;
        }
    }
}
