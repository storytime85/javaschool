package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    private int rows;

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        checkArrayLength(inputNumbers.size());
        checkArrayForNulls(inputNumbers);

        if (checkArrayLength(inputNumbers.size()) && checkArrayForNulls(inputNumbers)){
            Collections.sort(inputNumbers);
            return build(inputNumbers);
        } else {
            throw new CannotBuildPyramidException();
        }
    }

    private boolean checkArrayLength(int length){
        if (length < 0) {
            return false;
        }

        //initialize variables of quadratic equation
        int c = (-2 * length),
                b = 1, a = 1,
                d = (b * b) - (4 * a * c);
        if (d < 0)
            return false;

        //initialize roots of equation
        float root1 = ( -b + (float)Math.sqrt(d)) / (2 * a);
        float root2 = ( -b - (float)Math.sqrt(d)) / (2 * a);

        //if one of the roots is natural, use it as rows count, else return false and exception
        if (root1 > 0 && Math.floor(root1) == root1) {
            rows = (int)root1;
            return true;
        }
        if (root2 > 0 && Math.floor(root2) == root2) {
            rows = (int)root2;
            return true;
        }

        return false;
    }

    private boolean checkArrayForNulls(List<Integer> numbers){
        for (Integer i : numbers){
            if (i == null){
                return false;
            }
        }
        return true;
    }

    private int[][] build(List<Integer> numbers){

        int columns = (rows*2)-1;
        int[][] result = new int[rows][columns];
        int count =0;

        for (int i = 0; i<rows;i++){
            int shift = rows - i-1;
            for(int j = 0; j<=columns-shift;j++){
                if(!(j<shift)){
                    result[i][j++] = numbers.get(count++);
                }
            }
        }
        return result;
    }
}
